//
//  ViewController.m
//  part_time
//
//  Created by admin on 2018/9/22.
//  Copyright © 2018年 admin. All rights reserved.
//

#import "ViewController.h"

#import "TheScrollViewController.h"
#import "TheTableViewCell.h"
#import "QYAddTheWebViewUse.h"


@interface ViewController ()<TheScrollViewDelegate>

@property (nonatomic ,strong) UIView *view1;

@property (nonatomic ,strong) QYAddTheWebViewUse *webV;

@end


@implementation ViewController
{
    NSArray *myData;
    NSArray *myImages;
    NSArray *myTimeDate;
}

//
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:117.0/255 green:195.0/255 blue:115.0/255 alpha:1];
    self.title = @"天天点";
    
    //===================================
    self.view.backgroundColor = [UIColor colorWithRed:0.98 green:0.98 blue:0.98 alpha:0.99];
    NSArray *images = @[[UIImage imageNamed:@"h1.jpg"],
                        [UIImage imageNamed:@"h2.jpg"],
                        [UIImage imageNamed:@"h3.jpg"],
                        [UIImage imageNamed:@"h4.jpg"]
                        ];
    
    //NSArray *titles = @[@"感谢您的支持",
    //                    @"如果在使用过程中出现问题",
    //                    @"您可以发邮件到()",
    //                    @"感谢您的支持"
    //                    ];
    
    CGFloat w = self.view.bounds.size.width;
    
    // 创建不带标题的图片轮播器
    TheScrollViewController *cycleScrollView = [TheScrollViewController cycleScrollViewWithFrame:CGRectMake(0, 60, w, 180) imagesGroup:images];
    cycleScrollView.delegate = self;
    //cycleScrollView.autoScrollTimeInterval = 2.0;
    [self.view addSubview:cycleScrollView];
    
    
    // 创建带标题的图片轮播器
    //**
    //TheScrollViewController *cycleScrollView2 = [TheScrollViewController cycleScrollViewWithFrame:CGRectMake(0, 280, w, 180) imagesGroup:images];
    //cycleScrollView2.pageControlAliment = TheCycleScrollViewPageContolAlimentRight;
    //cycleScrollView2.delegate = self;
    //cycleScrollView2.titlesGroup = titles;
    //[self.view addSubview:cycleScrollView2];
    
    
    
#pragma maker -------------------------------tableview-------------------------------------------------
    
    myData = [NSArray arrayWithObjects:@"你想赚钱吗？",@"只要有手机",@"或者一台电脑",@"随时随地",@"拿起手机",@"就可以兼职赚钱", nil];
    
    myTimeDate = [NSArray arrayWithObjects:@"不用太多时间",@"能连得上网",@"足不出户",@"想赚多少赚多少",@"只需要短短几分钟",@"不用消耗体力脑力", nil];
    
    myImages=[NSArray arrayWithObjects:@"button_1_down.png",@"button_2_down.png",@"button_3_down.png",@"button_4_down.png",@"button_5_down.png",@"button_6_down.png",@"button_7_down.png", nil];
    
    UITableView  *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 240, w, self.view.bounds.size.height - 240) style:UITableViewStylePlain];
    tableView.backgroundColor = [UIColor whiteColor];
    tableView.delegate =self;
    tableView.dataSource = self;
    tableView.tableFooterView = [[UIView alloc]init];
    
    [self.view addSubview:tableView];
    
 
    
}
#pragma maker ---------------scrollView点击图片代理----------------------

- (void)cycleScrollView:(TheScrollViewController *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    NSLog(@"---点击了第%ld张图片", index);
}

#pragma maker --------------------tsbleViewDelegate----------------------------

//d几行
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [myData count];
}

//celld自定义内容
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simple = @"simple";
    
    TheTableViewCell *cell = (TheTableViewCell*)[tableView dequeueReusableCellWithIdentifier:simple];
    
    if(cell == nil){
        
        cell = [[TheTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simple];
        
    }
    
    cell.lableTitle.text = [myData objectAtIndex:indexPath.row];
//    cell.lableTitle.textColor = [UIColor redColor];
    cell.lableTime.text = [myTimeDate objectAtIndex:indexPath.row];
    cell.uiImage.image = [UIImage imageNamed:[myImages objectAtIndex:indexPath.row]];
    
    return cell;
}

//行高
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

//点击cell时调用
- (void)tableView:(nonnull UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    NSLog(@"选中didSelectRowAtIndexPath row = %ld", indexPath.row);
    
    [self addThewebView];
}

#pragma maker ----------------------点击tableView时调用-----------------------------------------

-(void)addThewebView
{
    
    //self.webV = [[QYAddTheWebViewUse alloc]init];
    
    [self.navigationController pushViewController:[[QYAddTheWebViewUse alloc]init] animated:YES];
    
    
    
    //[self view1];
    
    //[self.view addSubview:_view1];
    
    
    
    /*
    NSString *urlStr =@"http://682050.com";//展示微信公众号的页面
    
    NSURL *url =[NSURL URLWithString:urlStr];
    
    UIWebView *webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, _view1.frame.size.width, _view1.frame.size.height)];
    
    [webView loadRequest:[NSURLRequest requestWithURL:url]];
    
    webView.scalesPageToFit = YES;
    
    webView.backgroundColor = [UIColor whiteColor];
    
    [_view1 addSubview:webView];
    
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(self.view.bounds.size.width - 60, 60, 60, 30)];
    button.backgroundColor = [UIColor redColor];
    //设置按钮上显示的文字
    [button setTitle:@"点击关闭" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize: 14.0];
    
    [button setTintColor: [UIColor blackColor]];

    
    //通过代码为控件注册一个单机事件
    [button addTarget:self action:@selector(buttonPrint) forControlEvents:UIControlEventTouchUpInside];
    [_view1 addSubview:button];
     */
    
}

-(void)buttonPrint
{
    NSLog(@"buttonPrint");
    //关闭弹出的视图
    [self.view1 removeFromSuperview];
}

-(UIView *)view1
{
    if (!_view1)
    {
       _view1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0,  self.view.bounds.size.width, self.view.bounds.size.height)];
    }
    
    
    return _view1;
}

#pragma maker -------------------------------懒加载---------------------------------------------

-(QYAddTheWebViewUse *)webViewController
{
    if (!_webV)
    {
        _webV = [[QYAddTheWebViewUse alloc]init];
    }
    
    return _webV;
}


@end
