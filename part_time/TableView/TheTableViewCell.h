//
//  TheTableViewCell.h
//  part_time
//
//  Created by admin on 2018/9/22.
//  Copyright © 2018年 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TheTableViewCell : UITableViewCell

@property(nonatomic,strong) UIImageView *uiImage;
@property(nonatomic,strong) UILabel *lableTime;
@property(nonatomic,strong) UILabel *lableTitle;


@end

NS_ASSUME_NONNULL_END
