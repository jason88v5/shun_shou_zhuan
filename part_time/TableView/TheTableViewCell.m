//
//  TheTableViewCell.m
//  part_time
//
//  Created by admin on 2018/9/22.
//  Copyright © 2018年 admin. All rights reserved.
//

#import "TheTableViewCell.h"

@implementation TheTableViewCell

@synthesize uiImage = _uiImage;
@synthesize lableTime = _lableTime;
@synthesize lableTitle = _lableTitle;



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self uiImage];
        [self lableTitle];
        [self lableTime];
        
        [self addSubview:self.uiImage];
        [self addSubview:self.lableTitle];
        [self addSubview:self.lableTime];
    }
    return self;
}


- (UIImageView *)uiImage
{
    if (!_uiImage)
    {
        _uiImage = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 60, 60)];
    }
    
    return _uiImage;
}

- (UILabel *)lableTitle
{
    if (!_lableTitle)
    {
        _lableTitle = [[UILabel alloc]initWithFrame:CGRectMake(85, 15, 200, 15)];
    }
    
    _lableTitle.textColor = [UIColor colorWithRed:0.12 green:0.26 blue:0.9 alpha:1];
    
    return _lableTitle;
}

- (UILabel *)lableTime
{
    if (!_lableTime)
    {
        _lableTime = [[UILabel alloc]initWithFrame:CGRectMake(80, 50, 200, 20)];
    }
    _lableTime.font = [UIFont systemFontOfSize:13];
    _lableTime.textColor = [UIColor colorWithRed:0.17 green:0.47 blue:0.8 alpha:1];
        
    return _lableTime;
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}@end
