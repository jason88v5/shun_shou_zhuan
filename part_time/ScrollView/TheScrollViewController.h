//
//  TheScrollViewController.h
//  part_time
//
//  Created by admin on 2018/9/22.
//  Copyright © 2018年 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum {
    TheCycleScrollViewPageContolAlimentRight,
    TheCycleScrollViewPageContolAlimentCenter
} TheCycleScrollViewPageContolAliment;

@class TheScrollViewController;

@protocol TheScrollViewDelegate <NSObject>//自定义代理

- (void)cycleScrollView:(TheScrollViewController *)cycleScrollView didSelectItemAtIndex:(NSInteger)index;

@end


@interface TheScrollViewController : UIView

@property (nonatomic ,strong) NSArray *imagesGroup;//图片数组

@property (nonatomic ,strong) NSArray *titlesGroup;//标题数组

@property (nonatomic ,assign) CGFloat aotuScrollTimeInterval;//自动滚动视图的时间间隔

@property (nonatomic ,assign) TheCycleScrollViewPageContolAliment pageControlAliment;//

@property (nonatomic ,weak) id<TheScrollViewDelegate>delegate;//代理

+ (instancetype)cycleScrollViewWithFrame:(CGRect)frame imagesGroup:(NSArray *)imagesGroup;//工厂方法

@end

NS_ASSUME_NONNULL_END
