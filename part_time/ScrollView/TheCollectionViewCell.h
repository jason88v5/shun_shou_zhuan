//
//  ThwCollectionViewCell.h
//  part_time
//
//  Created by admin on 2018/9/22.
//  Copyright © 2018年 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TheCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) UIImageView *imageView;

@property (copy, nonatomic) NSString *title;


@end

NS_ASSUME_NONNULL_END
